#!/usr/bin/env python3
# versions.py -*-python-*-

import platform
import sys

import flask
import indago
import psycopg2
import sqlalchemy


class Versions():
    def info(self):
        output = []
        output.append(f'indago: {indago.__version__}')
        output.append(f'os: {platform.platform()}')
        output.append(f'python: {sys.version}')
        output.append(f'flask: {flask.__version__}')
        output.append(f'psycopg2: {psycopg2.__version__}')
        output.append(f'sqlalchemy: {sqlalchemy.__version__}')
        return output
