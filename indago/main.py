#!/usr/bin/env python3
# main.py -*-python-*-

import argparse
import getpass
import os
import sys
import time

import waitress
import werkzeug.security

import indago.config
import indago.db
import indago.dbload
import indago.sphinx
import indago.versions
import indago.wmain

# pylint: disable=unused-import
from indago.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE

def main():
    global indago # Because we will import indago.model below.

    parser = argparse.ArgumentParser(
        description=f'indago version {indago.__version__}')

    mgroup = parser.add_argument_group('Miscellaneous')
    mgroup.add_argument('--config', default=None, help='Configuration file')

    igroup = parser.add_argument_group(
        'Initialization and Teardown',
        'These options are destructive! Use with care!')
    igroup.add_argument('--init-user', action='store_true', default=False,
                        help='Initialize user database table')
    igroup.add_argument('--drop-user', action='store_true', default=False,
                        help='Drop the user database table')
    igroup.add_argument('--init-text', action='store_true', default=False,
                        help='Initialize all text-related database tables')
    igroup.add_argument('--drop-text', action='store_true', default=False,
                        help='Drop all text-related database tables')

    lgroup = parser.add_argument_group(
        'Load',
        'These options import data into the database')
    lgroup.add_argument('--load', nargs='+',
                        help='List of JSON files or directories of JSON files')

    sgroup = parser.add_argument_group(
        'Show',
        'These option are read-only and will not change any state.')
    sgroup.add_argument('--show-config', action='store_true', default=False,
                        help='Show configuration and exit')
    sgroup.add_argument('--show-user', action='store_true', default=False,
                        help='Show user database and exit')
    sgroup.add_argument('--show-meta', action='store_true', default=False,
                        help='Show sample of metadata table and exit')
    sgroup.add_argument('--show-content', action='store_true', default=False,
                        help='Show sample of content table and exit')
    sgroup.add_argument('--version', action='store_true', default=False,
                        help='Display version and exit')
    sgroup.add_argument('--info', action='store_true', default=False,
                        help='Get information about the database')

    qgroup = parser.add_argument_group(
        'Query',
        'These options support queries via the CLI')
    qgroup.add_argument('--query', default=None, help='Query')

    wgroup = parser.add_argument_group(
        'Web Server',
        'These options are for the web server')
    wgroup.add_argument('--server', action='store_true', default=False,
                        help='Start web server')
    wgroup.add_argument('--devserver', action='store_true', default=False,
                        help='Start flask internal web server for development')
    wgroup.add_argument('--host', default='127.0.0.1',
                        help='Interface for web server')
    wgroup.add_argument('--port', type=int, default=8888,
                        help='Port for web server')

    ugroup = parser.add_argument_group('Utility')
    ugroup.add_argument('--hash', action='store_true', default=False,
                        help='Generate a PBKDF2 hash')

    dgroup = parser.add_argument_group('Debugging')
    dgroup.add_argument('--debug', action='store_true', default=False,
                        help='Increase logging verbosity')
    dgroup.add_argument('--nodebug', nargs='+',
                        help='Modules for which debug is disables')
    dgroup.add_argument('--debughtml', action='store_true', default=False,
                        help='Display raw database values in html output')

    args = parser.parse_args()

    if len(sys.argv) == 1:
        parser.print_help()
        parser.exit()

    if args.debug:
        LOG_SET_LEVEL('DEBUG')
        LOG_SET_LEVEL('INFO', module='sqlalchemy.engine')
        if args.nodebug:
            for module in args.nodebug:
                if module.startswith('sqlalchemy'):
                    LOG_SET_LEVEL('ERROR', module=module)
                else:
                    LOG_SET_LEVEL('INFO', module=module)

    if args.version:
        versions = indago.versions.Versions()
        for line in versions.info():
            print(line)
        return 0

    if args.hash:
        pw1 = getpass.getpass('Enter password: ')
        pw2 = getpass.getpass('Re-enter password: ')
        if pw1 != pw2:
            ERROR('Passwords do not match')
        print(werkzeug.security.generate_password_hash(pw1))
        return 0

    config = indago.config.Config(args.config)

    if args.show_config:
         print('config')
         for section in config.config.sections():
             print(f'  [{section}]')
             for key, value in config.config[section].items():
                 print(f'    {key} = {value}')
#         print('\nflask')
#         app = indago.wmain.create_app(config.config)
#         for key, value in app.config.items():
#             print(f'  {key} = {value}')
         return 0

    db = indago.db.DB(config.config)

    if args.info:
        for line in db.info():
            print(line)
        return 0

    if args.drop_user:
        db.user_drop()
        return 0

    if args.init_user:
        db.user_init()
        return 0

    if args.drop_text:
        db.text_drop()
        return 0

    if args.init_text:
        db.text_init()
        return 0

    if args.show_user:
        # pylint: disable=consider-using-f-string
        print('{:>5s} {:<20s} {:<20s} {:<24s} {:<24s} {:>5s} {:<24s}'
              ' {:>5s} {:<24s} {:<24s} {}'.format(
                  'id', 'email', 'name', 'registered', 'confirmed', 'count',
                  'last_login', 'count', 'last_forgot', 'last_change',
                  'access'))
        session = indago.db.session()
        for user in session.query(indago.model.User).all():
            print('{:5d} {:<20s} {:<20s} {:<24s} {:<24s} {:5d} {:<24s}'
                  ' {:5d} {:<24s} {:<24s} {}'.format(
                      user.id,
                      user.email if user.email is not None else "",
                      user.name if user.name is not None else "",
                      time.ctime(user.registered)
                      if user.registered > 0 else "never",
                      time.ctime(user.confirmed)
                      if user.confirmed > 0 else "never",
                      user.login_count,
                      time.ctime(user.last_login)
                      if user.last_login > 0 else "never",
                      user.forgot_count,
                      time.ctime(user.last_forgot)
                      if user.last_forgot > 0 else "never",
                      time.ctime(user.last_change)
                      if user.last_change > 0 else "never",
                      user.access if user.access is not None else ""))
        return 0

    if args.show_meta:
        # pylint: disable=consider-using-f-stringa
        print('{:>12s} {:<20s} {:<10s} {:<20s} {}'.format(
            'document_id', 'basename', 'format', 'title', 'author'))
        session = indago.db.session()
        for meta in session.query(indago.model.Metadata).yield_per(200):
            try:
                print('{:12d} {:<20s} {:<10s} {:<20s} {}'.format(
                    meta.document_id,
                    meta.basename[:20] if meta.basename is not None else "",
                    meta.format if meta.format is not None else "",
                    meta.title[:20] if meta.title is not None else "",
                    str(meta.author) if meta.author is not None else ""))
            except BrokenPipeError:
                # Redirect stdout to /dev/null to avoid another
                # BrokenPipeError on exit.
                devnull = os.open(os.devnull, os.O_WRONLY)
                os.dup2(devnull, sys.stdout.fileno())
                return 0
        return 0

    if args.show_content:
        # pylint: disable=consider-using-f-stringa
        print('{:>12s} {:<12s} {:<4s} {:<4s} {}'.format(
            'line_id', 'document_id', 'page', 'line', 'text'))
        session = indago.db.session()
        for content in session.query(indago.model.Content).yield_per(200):
            try:
                print('{:12d} {:12d} {:4d} {:4d} {}'.format(
                    content.line_id,
                    content.document_id,
                    content.page,
                    content.line,
                    content.text[:50]))
            except BrokenPipeError:
                # Redirect stdout to /dev/null to avoid another
                # BrokenPipeError on exit.
                devnull = os.open(os.devnull, os.O_WRONLY)
                os.dup2(devnull, sys.stdout.fileno())
                return 0
        return 0

    if args.load:
        dbload = indago.dbload.DBLoad(args.load)
        dbload.load()
        return 0

    if args.query:
        sphinx = indago.sphinx.Sphinx(config.config, db)
        results = sphinx.query(args.query)
        for result in results:
            output = str(result['weight'])
            for key, value in result.items():
                if key == 'weight':
                    continue
                if key == 'metadata':
                    output += f'\n    title={value["title"]}'
                    continue
                output += f'\n    {key}={value}'
            print(output)
        return 0

    if args.server or args.devserver:
        app = indago.wmain.create_app(
            config.config, db,
            debug = args.debug if args.server else True,
            debughtml = args.debughtml)
        if args.server:
            waitress.serve(app, host=args.host, port=args.port,
                           url_scheme='https',
                           log_untrusted_proxy_headers=True,
                           trusted_proxy='localhost',
                           trusted_proxy_headers='x-forwarded-for' +
                           ' x-forwarded-host x-forwarded-port')
        else:
            # Development server: reloads on file changes.
            app.run(debug=args.debug, host=args.host, port=args.port,
                    ssl_context='adhoc')
        return 0

    parser.print_help()
    return -1
