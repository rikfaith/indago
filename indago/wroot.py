#!/usr/bin/env python3
# wroot.py -*-python-*-

import os
import time

import flask
import flask_login

import indago.sphinx
import indago.wmain

# pylint: disable=unused-import
from indago.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL

root = flask.Blueprint('root', __name__)


@root.route('/')
def index():
    query_string = ''
    query_results = ''
    elapsed = 0
    if flask.session.get('query_rendered', -1) is False:
        query_string = flask.session['query_string']
        start_time = time.time()
        query_results = indago.wmain.sphinx.query(query_string)
        elapsed = time.time() - start_time
        flask.session['query_rendered'] = False
    return flask.render_template('index.html',
                                 query_string=query_string,
                                 query_results=query_results,
                                 query_time=f'{elapsed:0.02f} seconds',
                                 debughtml=flask.current_app.config['DEBUGHTML'])


@root.route('/privacy')
def privacy():
    return flask.render_template('privacy.html')


@root.route('/preferences', methods=['GET', 'POST'])
def preferences():
    if not flask_login.current_user.is_authenticated:
        flask.flash('Must login to view preferences')
        return flask.redirect(flask.url_for('auth.login'))
    if flask.request.method == 'POST':
        error = None
        print(flask.request.form)
        flask.flash(error)
    return flask.render_template('preferences.html')


@root.route('/cooked/<path:file>')
def cooked(file):
    path = os.path.join('cooked', file)
    DEBUG(f'{path=}')
    if not os.path.exists(path):
        return flask.render_template('cooked.html',
                                     file_contents=f'{file} not available')
    with open(path, 'r', encoding='utf-8') as fp:
        file_contents = fp.read()

    return flask.render_template('cooked.html',
                                 file_contents=file_contents)

@root.route('/original/<path:file>')
def original(file):
    path = os.path.join('/raw', file)
    DEBUG(f'{file=} {path=}')
    return flask.render_template(
        'cooked.html',
        file_contents=f'<div class="container my-4"><img src={path}></div>')


@root.route('/raw/<path:path>')
def raw(path):
    DEBUG(f'{path=}')
    return flask.send_from_directory('../raw', os.path.basename(path))
