#!/usr/bin/env python3
# wmain.py -*-python-*-

import flask
import flask_login
import flask_mail
import flask_sqlalchemy

import indago.sphinx

app = flask.Flask(__name__, static_url_path='/static')
mail = flask_mail.Mail()
sphinx = None


def create_app(config, db, debug=False, debughtml=False):
    # pylint: disable=import-outside-toplevel, global-statement
    global sphinx

    app.debug = debug

    for key, value in config['flask'].items():
        app.config[key.upper()] = value

    for key, value in config['indago'].items():
        if key.startswith('hcaptcha') or key in {'title', 'favicon'}:
            app.config[key.upper()] = value

    app.config['DEBUGHTML'] = debughtml

    mail.init_app(app)

    import indago.db
    sphinx = indago.sphinx.Sphinx(config, db)

    import indago.model

    # The login manager
    login_manager = flask_login.LoginManager()
    login_manager.login_view = 'auth.login'
    login_manager.init_app(app)

    @login_manager.user_loader
    def load_user(user_id):
        return indago.model.User.query.get(int(user_id))

    # Support the cookie consent box
    @app.context_processor
    def inject_template_scope():
        injections = {}

        def cookies_check():
            value = flask.request.cookies.get('cookie-consent')
            return value == 'true'
        injections.update(cookies_check=cookies_check)
        return injections

    # We don't import these at the top because that causes a circular
    # dependency.
    import indago.wauth
    app.register_blueprint(indago.wauth.auth)
    import indago.wroot
    app.register_blueprint(indago.wroot.root)
    import indago.wsearch
    app.register_blueprint(indago.wsearch.search)

    return app
