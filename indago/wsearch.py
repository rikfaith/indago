#!/usr/bin/env python3
# wsearch.py -*-python-*-

import flask

# pylint: disable=unused-import
from indago.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL

search = flask.Blueprint('search', __name__)


@search.route('/query', methods=['POST'])
def query_post():
    INFO(flask.request.form.get('query', None))
    flask.session['query_string'] = flask.request.form.get('query', None)
    flask.session['query_rendered'] = False
    return flask.redirect(flask.url_for('root.index'))
