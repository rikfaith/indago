#!/usr/bin/env python3
# models.py -*-python-*-

import flask_login
from sqlalchemy import Column
from sqlalchemy import Integer
from sqlalchemy import String
from sqlalchemy import UnicodeText
from sqlalchemy import ForeignKey
from sqlalchemy.dialects.postgresql import ARRAY
from sqlalchemy.orm import relationship

import indago.db

# pylint: disable=unused-import
from indago.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE

if indago.db.base is None:
    FATAL('Must instantiate indago.db.DB before importing indago.model')

class User(indago.db.base, flask_login.UserMixin):
    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    email = Column(String(100), unique=True)
    name = Column(String(1000))
    password = Column(String(200))
    registered = Column(Integer)
    confirmed = Column(Integer)
    login_count = Column(Integer)
    last_login = Column(Integer)
    forgot_count = Column(Integer)
    last_forgot = Column(Integer)
    last_change = Column(Integer)
    access = Column(String(200))
    force_password_change = Column(Integer)

class Metadata(indago.db.base):
    __tablename__ = 'metadata'
    document_id = Column(Integer, primary_key=True)
    path = Column(UnicodeText)
    basename = Column(UnicodeText)
    mtime = Column(Integer)
    format = Column(String(10))
    title = Column(UnicodeText)
    author = Column(ARRAY(UnicodeText))
    publisher = Column(UnicodeText)
    subject = Column(ARRAY(UnicodeText))
    description = Column(ARRAY(UnicodeText))
    isbn = Column(UnicodeText)

class Content(indago.db.base):
    __tablename__ = 'content'
    line_id = Column(Integer, primary_key=True)
    document_id = Column(Integer, ForeignKey('metadata.document_id'))
    document = relationship('Metadata')
    page = Column(Integer)
    line = Column(Integer)
    line_in_work = Column(Integer)
    text = Column(UnicodeText)
