#!/usr/bin/env python3
# database.py -*-python-*-

import time

import psycopg2
from sqlalchemy import create_engine, text
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.engine.url import URL
from sqlalchemy.exc import ProgrammingError, OperationalError
from sqlalchemy.schema import DropTable
from sqlalchemy.ext.compiler import compiles

# pylint: disable=unused-import
from indago.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE

engine = None
session = None
base = None

# Since our tables may depend on other tables, make drop use cascade.
@compiles(DropTable, "postgresql")
def _compile_drop_table(element, compiler, **kwargs):
    return f'{compiler.visit_drop_table(element)} CASCADE'

class DB():
    def __init__(self, config, section='postgresql'):
        global engine
        global session
        global base

        self.config = config
        self.section = section

        if self.section not in self.config:
            FATAL(f'Configuration file missing the [{self.section}] section')

        dbconfig = { 'drivername': 'postgresql' }
        for key, value in self.config[self.section].items():
            if key in ['drivername', 'username', 'password', 'host', 'port',
                       'database', 'query']:
                dbconfig[key] = value
        url = URL.create(**dbconfig)

        self.engine = engine = create_engine(url)
        self.session = session = scoped_session(sessionmaker(autocommit=False,
                                                             autoflush=False,
                                                             bind=engine))
        self.base = base = declarative_base()
        base.query = session.query_property()

        import indago.model

    def _select(self, command, args=None, zeroth=True):
        results = []
        try:
            with self.engine.connect() as conn:
                cur = conn.execute(command, args)
                for result in cur:
                    results.append(result[0] if zeroth else result)
        except OperationalError:
            DECODE('Cannot connect')
            FATAL('Cannot connect')
        return results

    def _get_version(self):
        versions = self._select(text('select version()'))
        return versions[0]

    def _get_tables(self):
        tables = self._select(text(
            '''select tablename from pg_catalog.pg_tables where
            schemaname != 'pg_catalog' and
            schemaname != 'information_schema';'''))
        return tables

    def _get_table_size(self, table):
        sizes = self._select(text(
            '''select pg_size_pretty(pg_relation_size(:t));'''),
                             { 't': table })
        return sizes[0]

    def _get_table_count(self, table):
        counts = self._select(text(f'''select count(*) from {table};'''))
        return counts[0]

    def _get_table_description(self, table):
        columns = self._select(text(
            '''select column_name, data_type
            from information_schema.columns
            where table_name=:t
            order by ordinal_position;'''),
                               {'t': table},
                               zeroth=False)
        return columns

    def info(self):
        output = []
        output.append('Database')
        output.append(f'  {self._get_version()}')

        output.append('Tables')
        tables = self._get_tables()
        for table in tables:
            size = self._get_table_size(table)
            count = self._get_table_count(table)
            output.append(f'  {table:<30s} {size:<10s} {count:>d} entries')

        for table in tables:
            output.append(f'Description of {table}')
            columns = self._get_table_description(table)
            for column in columns:
                output.append(f'  {column[0]:<30s} {column[1]}')

        return output


    def user_add(self, email, name, password, access, force=False):
        import indago.model

        current_time = int(time.time())
        user = indago.model.User(
            email=email,
            name=name,
            password=password,
            registered=current_time,
            confirmed=current_time,
            login_count=0,
            last_login=0,
            forgot_count=0,
            last_forgot=0,
            last_change=0,
            access=access,
            force_password_change=0 if not force else 1)
        self.session.add(user)
        self.session.commit()

    def user_init(self):
        try:
            self.base.metadata.tables['user'].create(self.engine)
        except ProgrammingError as exception:
            if isinstance(exception.orig, psycopg2.errors.DuplicateTable):
                ERROR('The "user" table already exists')
            else:
                DECODE('Could not create "user" table')

        self.user_add('admin', 'admin', self.config['indago']['admin_hash'],
                      'admin', force=False)

    def user_drop(self):
        try:
            self.base.metadata.tables['user'].drop(self.engine)
        except ProgrammingError as exception:
            if isinstance(exception.orig, psycopg2.errors.UndefinedTable):
                ERROR('The "user" table does not exist')
            else:
                DECODE('Could not drop "user" table')

    def text_init(self):
        for table in self.base.metadata.tables:
            if table == 'user':
                continue
            try:
                self.base.metadata.tables[table].create(self.engine)
            except ProgrammingError as exception:
                if isinstance(exception.orig, psycopg2.errors.DuplicateTable):
                    ERROR(f'The "{table}" table already exists')
                else:
                    DECODE(f'Could not create "{table}" table')

    def text_drop(self):
        for table in self.base.metadata.tables:
            if table == 'user':
                continue
            try:
                self.base.metadata.tables[table].drop(self.engine)
            except ProgrammingError as exception:
                if isinstance(exception.orig, psycopg2.errors.UndefinedTable):
                    ERROR(f'The "{table}" table does not exist')
                else:
                    DECODE(f'Could not drop "{table}" table')

