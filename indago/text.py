#!/usr/bin/env python3
# text.py -*-python-*-

import re

import bs4
import nltk

# pylint: disable=unused-import
from indago.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE


class Text():
    STOPWORDS = ['the', 'of', 'is']
    SPAN = 'class="has-background-warning"'

    def __init__(self, query, result, max_len=200):
        self.query = query
        self.result = result
        self.max_len = max_len
        self.query_tokens = nltk.tokenize.word_tokenize(self.query)
        DEBUG(f'{self.query=}')
        DEBUG(f'{self.query_tokens=}')

    def mark(self, line):
        DEBUG(f'{line=}')
        escaped_query = re.escape(self.query)
        if re.search(escaped_query, line, flags=re.IGNORECASE):
            line = re.sub(r'(' + escaped_query + r')',
                          f'<span {Text.SPAN}>\\1</span>',
                          line, flags=re.IGNORECASE)
            DEBUG(f'{line=}')
            return line
        for token in set(self.query_tokens):
            line = re.sub(r'\b(' + re.escape(token) + r')\b',
                          f'<span {Text.SPAN}>\\1</span>',
                          line, flags=re.IGNORECASE)
        DEBUG(f'{line=}')

        # Merge marks on consecutive words.
        line = re.sub(r'</span>(.)' + f'<span {Text.SPAN}>', '\\1', line)

        # Remove remaining stopwords
        for stop in Text.STOPWORDS:
            line = re.sub(f'<span {Text.SPAN}>' + stop + '</span>',
                          stop, line, flags=re.IGNORECASE)
        return line

    def sample(self):
        if len(self.result) < self.max_len:
            DEBUG(f'{len(self.result)=} {self.max_len=}')
            return self.mark(self.result)

        result_tokens = nltk.tokenize.sent_tokenize(self.result)
        DEBUG(f'{result_tokens=}')

        scores = [0 for i in range(0, len(result_tokens))]
        for query in self.query_tokens:
            for idx, result in enumerate(result_tokens):
                if re.search(query, result, flags=re.IGNORECASE):
                    scores[idx] += 1
        DEBUG(f'{scores=}')

        output = ''
        found = 0
        for target in range(len(self.query_tokens),
                            len(self.query_tokens) // 2,
                            -1):
            for idx, score in enumerate(scores):
                if score == target:
                    output += ' ' + result_tokens[idx]
                    found += 1
            if found > 0:
                break

        if found == 0:
            last_space = self.result[:self.max_len-3].rfind(' ')
            if last_space > 0:
                output += self.result[:last_space] + '...'
            else:
                output += self.result[:self.max_len-3] + '...'

        output = re.sub(r' +', ' ', output).strip()

        # Make sure all the tags match up, since we're going to trust this as
        # valid html.
        soup = bs4.BeautifulSoup(output, 'html5lib')
        output = "".join([str(x) for x in soup.body.children])
        return self.mark(output)

