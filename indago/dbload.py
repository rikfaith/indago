#!/usr/bin/env python3
# dbload.py -*-python-*-

import json
import os
import time

import indago.db

# pylint: disable=unused-import
from indago.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE

class DBLoad():
    def __init__(self, path):
        import indago.model
        self.path = path
        self.metadata_objs = []

    def _listify(self, item):
        assert not isinstance(item, dict), f'{item=}'
        if item is None:
            return []
        if isinstance(item, tuple):
            item = item[0]
        if not isinstance(item, list):
            return [item]
        assert isinstance(item, list)
        assert item != [[]] and item != [None] and item != ['']
        return item

    def _fixup_data(self, data):
        if 'isbn' in data:
            if isinstance(data['isbn'], list):
                data['isbn'] = data['isbn'][0]
        return data

    def load_from_file(self, path, session):
        INFO(f'Loading {path}')
        with open(path, 'r', encoding='utf-8') as fp:
            data = json.load(fp)

        data = self._fixup_data(data)

        author = self._listify(data.get('author', None))
        subject = self._listify(data.get('subject', None))
        description = self._listify(data.get('description', None))
        content = data.get('content', None)
        metadata_obj = indago.model.Metadata(
            path=data.get('path', ''),
            basename=data.get('basename', ''),
            mtime=data.get('mtime', 0),
            format=data.get('format', ''),
            title=data.get('title', ''),
            author=author,
            publisher=data.get('publisher', ''),
            subject=subject,
            description=description,
            isbn=data.get('isbn', 0))

        if content is None or len(content) == 0:
            # There is no content, so we can delay the update.
            self.metadata_objs.append(metadata_obj)
            return

        session.add(metadata_obj)
        session.flush()
        content_objs = []
        for line in content:
            text = line.get('text', None)
            if text is None or text == '':
                # Skip empty lines
                continue
            content_obj = indago.model.Content(
                document_id=metadata_obj.document_id,
                page=line.get('page', -1),
                line=line.get('line', -1),
                line_in_work=line.get('line_in_work', -1),
                text=text)
            content_objs.append(content_obj)
        session.add_all(content_objs)

    def load_from_dir(self, path, session):
        INFO(f'Scanning directory {path}')
        with os.scandir(path) as iterator:
            for entry in iterator:
                fullpath = os.path.join(path, entry.name)
                if entry.name.startswith('.'):
                    DEBUG(f'Skipping "{fullpath}"')
                    continue
                if entry.is_file() and entry.name.endswith('.json'):
                    self.load_from_file(fullpath, session)
                if entry.is_dir():
                    self.load_from_dir(fullpath, session)
                DEBUG(f'Skipping "{fullpath}"')

    def load(self):
        session = indago.db.session
        for path in self.path:
            if os.path.isdir(path):
                self.load_from_dir(path, session)
            else:
                self.load_from_file(path, session)

        # Apparently, bulk_save_objects will be deprecated after 1.4?
        if len(self.metadata_objs) > 0:
            session.add_all(self.metadata_objs)
        session.commit()
