#!/usr/bin/env python3
# config.py -*-python-*-

import configparser
import os
import uuid

# pylint: disable=unused-import
from indago.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE


class Config():
    def __init__(self, paths=None):
        if paths is None:
            paths = ['/etc/indago', '~/.config/indago', '~/.indago']
        elif isinstance(paths, str):
            paths = [paths]

        self.paths = []
        for path in paths:
            self.paths.append(os.path.expanduser(path))

        self.config = configparser.ConfigParser()
        secret = uuid.uuid4().hex
        salt = uuid.uuid4().hex
        self.config.read_dict(
            {'indago': {'title': 'Indago',
                        'favicon': 'indago.ico'},
             'postgresql': {'host': 'localhost',
                            'database': 'indago',
                            'user': 'postgresql'},
             'flask': {'SQLALCHEMY_TRACK_MODIFICATIONS': False,
                       'TEMPLATES_AUTO_RELOAD': True,
                       'SECRET_KEY': secret,
                       'SECURITY_PASSWORD_SALT': salt,
                       # 'SESSION_COOKIE_SECURE': True,
                       'REMEMBER_COOKIE_SECURE': True,
                       'SESSION_COOKIE_HTTPONLY': True,
                       'REMEMBER_COOKIE_HTTPONLY': True}
             })
        self.config.read(self.paths)

        error = ''
        if not self.config.has_section('indago') or \
           'admin_hash' not in self.config['indago']:
            error += '''
    The configuration file (e.g., ~/.indago) must have a section called
    [indago] with an "admin_hash" key. The value of the key is a hash of the
    admin password, generated using the --hash option.'''

#        if not self.config.has_section('indago') or \
#           'hcaptcha_sitekey' not in self.config['indago'] or \
#           'hcaptcha_secretkey' not in self.config['indago']:
#            error += '''
#    The configuration file (e.g., ~/.indago) must have a section called
#    [indago] with "hcaptcha_sitekey" and "hcaptcha_secretkey" keys. The value
#    of these keys are from the hCaptcha.com web site.'''

        if 'password' not in self.config['postgresql']:
            error += '''
    The configuration file (e.g., ~/.indago) must have a section called
    [postgresql] with a "password" key. This password will be used with the
    "host", "database", and "user" keys to access the text database.'''

#        if 'mail_default_sender' not in self.config['flask']:
#            error += '''
#    The configuration file (e.g., ~/.indago) must have a section called
#    [flask] with a "mail_default_sender" key. Confirmation email will be
#    sent using this as the return address.'''

        if error != '':
            FATAL(error)

        if self.config['flask']['SECRET_KEY'] == secret or \
           self.config['flask']['SECURITY_PASSWORD_SALT'] == salt or \
           self.config['flask']['SECRET_KEY'] == \
           self.config['flask']['SECURITY_PASSWORD_SALT']:
            INFO('''
    The configuration file (e.g., ~/.indago) should have different unique
    random values in the [flask] section for SECRET_KEY and for
    SECURITY_PASSWORD_SALT. Otherwise, sessions will not work correctly.''')
