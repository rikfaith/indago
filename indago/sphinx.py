#!/usr/bin/env python3
# sphinx.py -*-python-*-

import re

import flask
import pymysql

import indago.text

# pylint: disable=unused-import
from indago.log import LOG_SET_LEVEL, DEBUG, INFO, ERROR, FATAL, DECODE


class Sphinx():
    def __init__(self, config, db, section='sphinxsearch'):
        self.config = config
        self.db = db
        self.section = section
        self.conn = None

        if self.section not in self.config:
            FATAL(f'Configuration file missing the [{self.section}] section')

        self.params = {}
        for key, value in self.config[self.section].items():
            if key in ['port']:
                self.params[key] = int(value)
            else:
                self.params[key] = value

    @staticmethod
    def _connect(params):
        # pylint: disable=broad-except
        try:
            conn = pymysql.connect(**params)
        except (Exception, pymysql.DatabaseError):
            DECODE('Cannot connect to database')
            return None

        return conn

    def _execute(self, commands, args=None, conn=None):
        # pylint: disable=broad-except
        retcode = True

        if conn is None:
            conn = self._connect(self.params)

        cur = conn.cursor()
        try:
            for command in commands:
                INFO('command=%s args=%s', command, args)
                cur.execute(command, args)
        except (Exception, pymysql.DatabaseError):
            DECODE('Command failed')
            retcode = False

        return retcode, conn, cur

    def _row_to_dict(self, row):
        result = {}
        if row is None:
            return result
        for column in row.__table__.columns:
            result[column.name] = getattr(row, column.name)
        return result

    def query(self, query_string, table_map=None):
        '''
        table_map is a list of tuples, where each tuple maps the name of the
        sphinx table to the sqlalchemy model that contains the actual
        contents. The third item spcifies the primary key in the table. The
        fourth and fifth items, if not None, specify how to obtain metadata
        for the entry.

        '''
        query_string = query_string.strip()
        session = self.db.session()
        if table_map is None:
            table_map = [
                ('indago_metadata', indago.model.Metadata, 'document_id',
                 None, None),
                ('indago_content', indago.model.Content, 'line_id',
                 'document_id', indago.model.Metadata)
            ]

        results = []
        conn = None
        for sphinx_table, model, primary_key, m_key, m_model in table_map:
            retcode, conn, cur = self._execute(
                [f'select id, weight() from {sphinx_table} where match(%s);'],
                (query_string,),
                conn=conn)
            if retcode:
                for key, weight in cur:
                    INFO(f'{key=} {weight=} {getattr(model, primary_key)=}')
                    row = session.query(model).filter(
                        getattr(model, primary_key)==key).first()
                    description = self._row_to_dict(row)
                    description['weight'] = weight
                    description['table'] = model.__tablename__

                    if m_key in description and m_model is not None:
                        # Obtain metadata
                        value = description[m_key]
                        INFO(f'{m_key=} {description[m_key]=}')
                        row = session.query(m_model).filter(
                            getattr(m_model, m_key)==value).first()
                        if row is not None:
                            description['metadata'] = self._row_to_dict(row)

                    # Make sure there is a title and author in the metadata.
                    if 'metadata' in description:
                        if 'title' not in description['metadata']:
                            description['metadata']['title'] = '???'
                        if 'author' not in description['metadata']:
                            description['metadata']['author'] = []

                    # Get a text sample.
                    if 'text' in description:
                        # For content.
                        text = indago.text.Text(query_string,
                                                description['text'])
                        description['sample'] = text.sample()
                    elif 'description' in description:
                        # For metadata.
                        text = indago.text.Text(
                            query_string, ' '.join(description['description']))
                        description['sample'] = text.sample()
                    else:
                        description['sample'] = ''

                    # Generate a reference for the sample.
                    ref = ''
                    if 'page' in description:
                        ref += f'page {description["page"]}'
                        if 'line' in description:
                            if ref != '':
                                ref += ', '
                            ref += f'line {description["line"]}'
                    description['reference'] = ref

                    results.append(description)
                cur.close()
        conn.close()
        return results
