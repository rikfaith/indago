# indago

Indago is a suite of tools for indexing and searching text.

The text will have already been pre-processed by a different tool (e.g.,
lustro) into JSON format.

The following technologies are use:
* Python 3, with Flask and Bulma
* PostgreSQL (postgresql.org)
* Sphinx (sphinxsearch.com)

## Getting Started

### Packages

As root, install these packages:
* apt-get install python3-jsbeautifier
* apt-get install python3-waitress
* apt-get install python3-werkzeug
* apt-get install python3-psycopg2
* apt-get install python3-pymysql
* apt-get install python3-nltk
* apt-get install python3-flask
* apt-get install python3-flask-login
* apt-get install python3-flask-mail
* apt-get install python3-zxcvbn
* apt-get install python3-lxml
* apt-get install python3-sqlalchemy
* apt-get install python3-sqlalchemy-utils
* apt-get install python3-waitress

### Postgres

AS ROOT, install and configure postgresql:

```bash
apt-get install postgresql
su -c "psql" - postgres
```

At the "postgres=#" prompt, enter "\password" and enter the new password
(e.g., from mcookie(1)).


Then, in the ~/.indago config file, make sure the following section is
present:

~~~
    [postgresql]
    host=localhost
    database=indago
    user=postgres
    password=<password>
~~~

AS ROOT, install sphinxsearch:

```bash
apt-get install sphinxsearch
```

and then update /etc/sphinxsearch/sphinx.conf from the sphinx.conf.example file in the
git repository -- making sure to put in the postgreq password in all THREE
places!

Also update /etc/group to put the desired non-root user in the sphinxsearch
group.

# Data

As a normal user, initialize the user database if required:

```bash
indago --init
```

FIXME: add note about processing into JSON.

and then load the data:

```bash
indago --load
```

and then make a directory for sphinxsearch:
```bash
sudo mkdir /var/local/indago
sudo chown sphinxsearch.sphinxsearch /var/local/indago
sudo chmod 4775 /var/local/indago

and then update sphinxsearch, making sure you are in the sphinxsearch group (which, if you just created it, will require a logout/login):

```bash
indexer --all --rotate
```

This probably won't rotate the indices because sphinxd is running as
sphinxsearch and the user running indexer cannot signal the other process. To
work around this with sudo, add the following line to /etc/sudoers:

```bash
OTHER-USER ALL=(sphinxsearch) NOPASSWD:/usr/bin/indexer
```

Where OTHER-USER is the username of the user that will run the indexer as
follows:

```bash
sudo -u sphinxsearch indexer --all --rotate
```

Alternatively, run sphinxd as the same user that runs in indexer.

Then, in the ~/.indago config file, make sure the following section is
present:
~~~
    [sphinxsearch]
    host=localhost
    port=9306
~~~

## Updating Bulma

* curl -O https://github.com/jgthms/bulma/releases/download/0.9.3/bulma-0.9.3.zip
* unzip bulma-0.9.3.zip
* copy bulma/css/bulma.min.css to the indago/static subdirectory

## Updating Bulma tooltips

* curl -O
  https://raw.githubusercontent.com/CreativeBulma/bulma-tooltip/master/dist/bulma-tooltip.min.css
* move bulma-tooltip.min.css to the indago/static subdirectory


## Downloading NLTK data

From python3:
* import nltk
* nltk.download('punkt')
